import React, { createContext, useState } from "react";

export const ProductContext = createContext(null);

export const ProductProvider = ({ children }) => {
    const [selectedCategory, setSelectedCategory] = useState('all');
    const [searchTerm, setSearchTerm] = useState('');
    const [noDataMsg, showNoDataMsg] = useState(false);

    return (
        <ProductContext.Provider value={{
            selectedCategory,
            setSelectedCategory,
            searchTerm,
            setSearchTerm,
            noDataMsg,
            showNoDataMsg
        }}>
            {children}
        </ProductContext.Provider>
    )
}
