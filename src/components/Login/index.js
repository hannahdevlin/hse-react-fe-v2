import React, { useState } from "react";
import { Link } from "react-router-dom";
import Header from "../Header/index";
import CustomButton from "../Common/CustomButton";
import TextInput from "../Common/Form/TextInput";
import arrowBackIcon from "../../assets/svgs/arrow-back-icon.svg";
import "./style.css";

function Login () {
    const [username, setUsername] = useState(null);
    const [password, setPassword] = useState(null);

    return (
        <div>
          <div className="login-container">
            <Link style={{ color: '#0048a8', display: 'flex' }} to='#'>
              <img src={arrowBackIcon} alt='arrowBackIcon' width={15} />
              <span style={{ padding: '0px 8px' }}>Go back</span>
            </Link>
          </div>
          <Header title="Sign into your account" />
          <div className="login-form">
              <div style={{ width: '65%', border: '1px solid #aeb7be', marginBottom: 26 }} />
                <div style={{ width: '65%', marginBottom: 26 }}>
                  <TextInput
                    legend="Username"
                    type="text"
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                    />
                </div>
                <div style={{ width: '65%' }}>
                  <TextInput 
                    legend="Password"
                    type="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    />
                </div>
                <div style={{ width: '65%', border: '1px solid #aeb7be', marginTop: 26 }} />
                <div className='login-form-btn'>
                  <CustomButton
                    action={'/products'}
                    disabled={!username || !password}
                    content="Sign in"
                    style={{ width: 0 }}/>
                    <div style={{ marginTop: 26 }}>
                      <Link style={{ color: '#0048a8', textDecoration: 'underline'}} to='#'>
                        I forgot my username or password
                      </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login;