import React from "react";
import { Link } from "react-router-dom";
import styles from "./Navbar.module.css";
import hseLogo from '../../assets/svgs/hse-logo.svg';
import { connect } from "react-redux";

const Navbar = () => {
  return (
    <div className={styles.navbar}>
      <div style={{ width: '65%', display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          <span style={{ paddingRight: 17 }}>
            <img src={hseLogo} width={50}/>
          </span>
          <h3 style={{ color: 'white', fontSize: 21, paddingTop: 9 }}>View and order publications</h3>
        </div>
        <div style={{ paddingTop: 13 }}> 
          <Link className={styles.sign_in_link} to="/login">Sign into your account</Link>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    cart: state.shop.cart,
  };
};

export default connect(mapStateToProps)(Navbar);
