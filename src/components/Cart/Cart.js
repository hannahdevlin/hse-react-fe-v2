import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import CartItem from "./CartItem/CartItem";
import Header from "../Header";
import arrowBackIcon from "../../assets/svgs/arrow-back-icon.svg"
import CustomButton from "../Common/CustomButton";

import styles from './Cart.module.css';

const Cart = ({ cart }) => {
  const [totalItems, setTotalItems] = useState(0);

  useEffect(() => {
    let items = 0;

    cart.forEach((item) => {
      items += item.qty;
    });

    setTotalItems(items);
  }, [cart, totalItems, setTotalItems]);

  return (
    <div>
      <div className={styles.cart}>
        <Link style={{ color: '#0048a8', display: 'flex' }} to='/products'>
          <img src={arrowBackIcon} alt='arrowBackIcon' width={15} />
          <span style={{ padding: '0px 8px' }}>Go back</span>
        </Link>
      </div>
        <Header title="Your basket" />
        <div className={styles.cart_items}>
          <div style={{ width: '65%', border: '1px solid #aeb7be' }} />
          <div style={{ width: '65%' }}>
              {cart.map((item) => (
                <CartItem key={item.id} item={item} />
              ))}
            </div>
          <div className={styles.cart_checkout_btn} style={{ display: cart.length ? 'flex' : 'none', }}>
            <CustomButton action={'/user-details-edit'} content="Continue to order" />
          </div>
        </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    cart: state.shop.cart,
  };
};

export default connect(mapStateToProps)(Cart);
