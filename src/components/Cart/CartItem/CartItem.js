import React, { useState } from "react";
import styles from "./CartItem.module.css";
import { Link } from "react-router-dom";

import { connect } from "react-redux";
import {
  adjustItemQty,
  removeFromCart,
} from "../../../redux/Shopping/shopping-actions";

const CartItem = ({ item, adjustQty, removeFromCart }) => {
  const [input, setInput] = useState(item.qty);

  const onChangeHandler = (e) => {
    setInput(e.target.value);
    adjustQty(item.id, e.target.value);
  };

  return (
    <div style={{
      borderBottom: '2px solid #aeb7be',
      padding: '15px 0px',
      width: '100%'
      }}>
      <div className={styles.cartItem__details}>
        <p className={styles.details__title}>{item.title}</p>
      </div>
      <div className={styles.cartItem__actions}>
        <div style={{ width: '30px' }}>
          <label htmlFor="qty" style={{ color: '#212b32' }}>Quantity</label>
          <input
            min="1"
            type="number"
            id="qty"
            name="qty"
            value={input}
            className={styles.quantityInput}
            onChange={onChangeHandler}
          />
        </div>
        <Link
          to='#'
          style={{
            textDecoration: 'underline',
            color: 'rgb(0, 72, 168)',
            paddingTop: 28
          }}
          onClick={() => removeFromCart(item.id)}
          >
            Remove
        </Link>
      </div>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    adjustQty: (id, value) => dispatch(adjustItemQty(id, value)),
    removeFromCart: (id) => dispatch(removeFromCart(id)),
  };
};

export default connect(null, mapDispatchToProps)(CartItem);
