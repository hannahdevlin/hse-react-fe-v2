import React from "react";
import Header from "../Header/index";
import CustomButton from "../Common/CustomButton";

function OrderConfirmed () {
    return (
        <div>
            <Header title="Your order has been confirmed" />
            <div className="details">
            <div className='confirm-order-btn'>
                <CustomButton action="/products" content="Browse more publications" />
            </div>
            </div>
        </div>
    )
}

export default OrderConfirmed;