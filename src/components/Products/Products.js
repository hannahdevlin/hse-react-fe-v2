import React, { useEffect, useState, useContext } from "react";
import { useSelector } from 'react-redux';
import { Link } from "react-router-dom";
import Header from "../../components/Header";
import HealthPromoSearch from '../../components/Health-Promo-Search-Pubs';

import styles from "./Products.module.css";
import Product from "./Product/Product";
import { ProductContext } from "../../context/ProductContext";
import basketIcon from "../../assets/svgs/shopping-basket-icon.svg";

// Redux
import { connect } from "react-redux";
import { fetchProducts } from "../../redux/Shopping/shopping-actions";

function Products ({ products, fetchProducts }) {
  const {
    selectedCategory,
    searchTerm,
    noDataMsg,
    showNoDataMsg
  } = useContext(ProductContext);

  const cart = useSelector((state) => state.shop.cart);
  const [searchResults, setSearchResults] = useState([]);
  const [cartCount, setCartCount] = useState(0);
  
  let promoTitle = 'Health promotion publications';
  let promoSubTitle = 'Walk-in test centres are usually for people who do not have symptoms. Today priority will be given to people who have symptoms or are close contacts. You do not need an appointment or a referral from a GP.';

  useEffect(() => {
    fetchProducts();
  }, []);

  useEffect(() => {
    // find products with selected category
    let result = products.filter((prod) => String(prod.categories) === selectedCategory);
    if (searchTerm) {
      setSearchResults(result.filter((resultItem) => resultItem.title.includes(searchTerm)))
    } else {
      setSearchResults(result);
    }
  }, [selectedCategory, searchTerm]);

  useEffect(() => {
    // update cart counter (your basket)
    let count = 0;
    if (cart) {
      cart.forEach((item) => {
        count += item.qty;
      });
      setCartCount(count);
    } else {
      showNoDataMsg(true);
    }
  }, [cart, cartCount]);

  return (
    <div>
      <Header title={promoTitle} subTitle={promoSubTitle} showBtn={true} />
      <HealthPromoSearch />
      <div style={{ width: '100%', alignItems: 'center', display: 'flex', flexDirection: 'column', marginTop: '3rem' }}>
        <div style={{ borderBottom: '2px solid #aeb7be', width: '65%', display: 'flex', flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 20 }}>
          <div style={{ width: '70%', paddingTop: 5 }}>
            <span style={{ display: searchResults.length ? '' : 'none', color: '#425462' }}>
              Your search returned {searchResults?.length} results
            </span>
          </div>
          <div style={{ display: 'flex' }}>
            <div>
              <img src={basketIcon} alt="basket-icon" width={30} />
            </div>
            <div style={{ paddingTop: 8, paddingLeft: 8 }}>
              <span style={{ textDecoration: 'underline' }}>
                <Link style={{ color: '#0048a8', fontWeight: 700 }} to="/cart">
                  Your basket ({ cartCount })
                </Link>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div className={styles.products}>
        {
          searchResults.length && searchResults !== null
          ? searchResults?.map((product) => (
          <Product key={product.id} product={product} />
          ))
          : searchTerm && !searchResults.length ? 
          products.filter(prod => prod.title.includes(searchTerm)).map((product) => (
          <Product key={product.id} product={product} />
          ))
          : products.map((product) => (
            <Product key={product.id} product={product} />
          ))
        }
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    products: state.shop.products,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchProducts: () => dispatch(fetchProducts())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Products);
