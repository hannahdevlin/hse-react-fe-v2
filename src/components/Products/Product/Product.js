import React, { useState } from "react";
import { Link } from "react-router-dom";
import styles from "./Product.module.css";

// Redux
import { connect } from "react-redux";
import {
  loadCurrentItem,
  addToCart,
  adjustItemQty
} from "../../../redux/Shopping/shopping-actions";

import pdfLogo from '../../../assets/svgs/pdf-icon.svg'

const Product = ({ product, addToCart }) => {
  const [input, setInput] = useState(0);
  const [descVisible, setDescVisible] = useState(false);

  const onChangeHandler = (e) => {
    setInput(parseInt(e.target.value));
  };

  const addItem = () => {
    addToCart({ id: product.id, input })
    setTimeout(function() {
      setInput(0);
    }, 1000)
  }

  function truncate (desc) {
    let sub = desc.substring(0, 110);
    let remainder = desc.substring(110);
    return (
      <div>
        {sub}
        <Link
          onClick={() => setDescVisible(true)}
          style={{
            display: descVisible ? 'none' : '',
            textDecoration: 'underline',
            color: '#0048a8'
          }}> 
          ...read more
        </Link>
        {descVisible ? remainder : ''}
        <Link
          onClick={() => setDescVisible(false)}
          style={{
            display: descVisible ? '' : 'none',
            textDecoration: 'underline',
            color: '#0048a8'
          }}> 
          ...show less
        </Link>
      </div>
    );
  }

  return (
    <div className={styles.productItemContainer} style={{ display: !product.title ? 'none' : 'flex' }}>
      <div style={{ width: '75%' }}>
        <div className={styles.productItemHeader}>
          {product.title}
        </div>
        <div className={styles.productItemDesc} style={{ display: product.description ? '' : 'none' }}>
          {product.description.length >= 110
            ? truncate(product.description)
            : product.description}
        </div>
        <div className={styles.productItemDetail}>
          <div id={styles.detail} style={{ display: product.order_code ? '' : 'none' }}>
            <span style={{ fontWeight: 'bold', marginRight: 8 }}>Order Code: </span>
            <span style={{ color: '#425462' }}>{product.order_code}</span>
          </div>
          <div id={styles.detail}>
            <span style={{ fontWeight: 'bold', marginRight: 8, display: product.categories ? '' : 'none' }}>Topic: </span>
            <span style={{ color: '#425462' }}>{product.categories}</span>
          </div>
          <div id={styles.detail}>
            <span style={{ fontWeight: 'bold', marginRight: 8 }}>Maximum order: </span>
            <span style={{ color: '#425462' }}>100</span>
          </div>
        </div>
        <div className={styles.productItemInputs}>
          <div style={{ width: '7%', paddingTop: 5 }}>
            <div style={{ width: '30px' }}>
              <label htmlFor="qty" style={{ color: '#212b32' }}>
                Quantity
              </label>
              <input
                min="1"
                type="number"
                id="qty"
                name="qty"
                value={input}
                onChange={onChangeHandler}
                className={styles.qtyInput}
              />
            </div>
          </div>
          <div style={{ width: '7%', marginLeft: -50 }}>
            <button
              onClick={() => addItem(product.id)}
              className={`${styles.buttons__btn} ${styles.buttons__add}`}
              style={{ margin: 'auto', marginTop: 17 }}
              disabled={!input}
            >
              Add to cart
            </button>
          </div>
          <div style={{ display: 'flex', flexDirection: 'row', width: '70%' }}>
            <img src={pdfLogo} alt='pdf-logo' width={20} style={{ margin: '23px 9px 5px' }} />
            <Link
              to='#'
              style={{
                paddingTop: 32,
                textDecoration: 'underline',
                color: '#0048a8',
                fontWeight: 600
              }}>
              {product.title}, 200KB, 15 pages
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (id) => dispatch(addToCart(id)),
    loadCurrentItem: (item) => dispatch(loadCurrentItem(item)),
    adjustQty: (id, value) => dispatch(adjustItemQty(id, value)),
  };
};

export default connect(null, mapDispatchToProps)(Product);
