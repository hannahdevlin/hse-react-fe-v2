import React from 'react';
import CustomDropdown from '../Common/CustomDropdown';
import SearchBar from '../Common/SearchBar';
import './style.css';

export default function HealthPromoSearch () {
    return (
        <div style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            }}>
            <div style={{ width: '65%' }}>
                <div className='HealthPromoTitle'>
                    Search for publications
                </div>
                <div
                    className='HealthPromoInputs'
                    style={{
                        display: 'flex',
                        flexDirection: window.innerWidth <= 955 ? 'column' : 'row',
                        minWidth: '550px',
                        }}>
                        <CustomDropdown />
                        <SearchBar />
                </div>
            </div>
        </div>
    )
}