import React from 'react';
import CustomButton from '../Common/CustomButton';
import './style.css';

export default function Header ({ title, subTitle, showBtn }) {
    return (
        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
            <div className='headerContainer'>
                <div className='headerText'>
                    {title}
                </div>
                <div
                    className="headerSubText"
                    style={{
                        width: '70%',
                        marginBottom: 20,
                        paddingTop: '36px',
                        display: subTitle ? 'flex' : 'none'
                        }}>
                    {subTitle}
                </div>
                <div style={{ display: showBtn ? 'flex' : 'none' }}>
                    <CustomButton content="Sign into your account" action="/login" />
                </div>
            </div>
        </div>
        
    )
}