import React from "react";
import { Link } from "react-router-dom";
import Header from "../Header/index";
import CustomButton from "../Common/CustomButton";
import arrowBackIcon from "../../assets/svgs/arrow-back-icon.svg";
import './style.css';

function UserDetails () {
    return (
        <div>
          <div className="details-container">
            <Link style={{ color: '#0048a8', display: 'flex' }} to='#'>
              <img src={arrowBackIcon} alt='arrowBackIcon' width={15} />
              <span style={{ padding: '0px 8px' }}>Go back</span>
            </Link>
          </div>
          <Header title="Your details" />
          <div className="details">
            <div style={{ width: '65%', border: '1px solid #aeb7be', marginBottom: 26 }} />
            User details here ..
            <div style={{ width: '65%', border: '1px solid #aeb7be', marginTop: 26 }} />
            <div className='confirm-order-btn'>
                <CustomButton content="Confirm order" style={{ width: 0 }}/>
            </div>
          </div>
        </div>
    )
}

export default UserDetails;