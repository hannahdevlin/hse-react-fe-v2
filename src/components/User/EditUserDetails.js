import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Header from "../Header/index";
import CustomButton from "../Common/CustomButton";
import TextInput from "../Common/Form/TextInput";
import arrowBackIcon from "../../assets/svgs/arrow-back-icon.svg";
import './style.css';

function EditUserDetails() {
    const [firstname, setFirstname] = useState(null);
    const [lastname, setLastname] = useState(null);
    const [email, setEmail] = useState(null);
    const [phone, setPhone] = useState(null);
    const [address, setAddress] = useState(null);
    const [address2, setAddress2] = useState(null);
    const [city, setCity] = useState(null);
    const [county, setCounty] = useState(null);

    const [emptyField, setEmptyField] = useState(true);
    const formElements = [firstname, lastname, email, phone, address, address2, city, county];

    useEffect(() => {
      const valCheck = formElements.map((el) =>  el);

      if (valCheck.includes(null) || valCheck.includes('')) {
        setEmptyField(true);
      }
      else if (!valCheck.includes(null) || valCheck.includes('')) {
        setEmptyField(false);
      }

    }, [formElements]);

    return (
        <div>
            <div className="details-container">
              <Link style={{ color: '#0048a8', display: 'flex' }} to='/cart'>
                <img src={arrowBackIcon} alt='arrowBackIcon' width={15} />
                <span style={{ padding: '0px 8px' }}>Go back</span>
              </Link>
            </div>
            <Header title="Edit your details" />
            <div className="details">
                <div style={{ width: '65%', border: '1px solid #aeb7be', marginBottom: 26 }} />
                  <div style={{ width: '65%', marginBottom: 26 }}>
                    <TextInput
                      legend="First name"
                      type="text"
                      value={firstname}
                      onChange={(e) => setFirstname(e.target.value)}
                      required={true}
                    />
                  </div>
                  <div style={{ width: '65%', marginBottom: 26 }}>
                    <TextInput
                      legend="Last name"
                      type="text"
                      value={lastname}
                      onChange={(e) => setLastname(e.target.value)}
                    />
                  </div>
                  <div style={{ width: '65%', marginBottom: 26 }}>
                    <TextInput
                      legend="Email"
                      type="email"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                    />
                  </div>
                  <div style={{ width: '65%', marginBottom: 26 }}>
                    <TextInput
                      legend="Phone number"
                      type="number"
                      value={phone}
                      onChange={(e) => setPhone(e.target.value)}
                    />
                  </div>
                  <div style={{ width: '65%', marginTop: 26, marginBottom: 26 }}>
                    <h2>Delivery address</h2>
                  </div>
                  <div style={{ width: '65%', marginBottom: 26 }}>
                    <TextInput
                      legend="Address"
                      type="text"
                      value={address}
                      onChange={(e) => setAddress(e.target.value)}
                    />
                  </div>
                  <div style={{ width: '65%', marginBottom: 26 }}>
                    <TextInput
                      legend="Address line 2"
                      type="text"
                      value={address2}
                      onChange={(e) => setAddress2(e.target.value)}
                    />
                  </div>
                  <div style={{ width: '65%', marginBottom: 26 }}>
                    <TextInput
                      legend="City"
                      type="text"
                      value={city}
                      onChange={(e) => setCity(e.target.value)}
                    />
                  </div>
                  <div style={{ width: '65%', marginBottom: 26 }}>
                    <TextInput
                      legend="County"
                      type="text"
                      value={county}
                      onChange={(e) => setCounty(e.target.value)}
                    />
                  </div>
                <div style={{ width: '65%', border: '1px solid #aeb7be', marginTop: 26 }} />
                <div className='confirm-order-btn'>
                  <CustomButton
                    action='/order-confirmed'
                    content="Confirm order"
                    style={{ width: 0 }}
                    disabled={emptyField}
                  />
                </div>
            </div>
        </div>
    )
}

export default EditUserDetails;