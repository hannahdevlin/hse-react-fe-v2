import React from 'react';
import './style.css';

export default function CustomButton ({ content, disabled, action }) {
    return (
        <div>
          <form action={action}>
            <input className='customButton' type='submit' value={content} disabled={disabled} />
          </form>
        </div>
    )
}