import React from "react";
import "./style.css";

function TextInput({ legend, type, value, onChange, required }) {
    return (
        <div>
          <div>
            <div className="input-legend">
                {legend}
            </div>
            <div style={{ display: 'flex' }}>
              <input
                className='input-box'
                value={value}
                onChange={onChange}
                type={type}
                required={required}
                />
            </div>
          </div>
        </div>
    )
}

export default TextInput;