import React, { useContext } from 'react';
import { useSelector } from "react-redux";
import { ProductContext } from '../../../context/ProductContext';
import './style.css';

function CustomDropdown () {
    const categories = useSelector((state) => state.shop.categories);
    const { setSelectedCategory, selectedCategory } = useContext(ProductContext);

    const onOptSelect = (newVal) => {
        setSelectedCategory(newVal);
    }

    return (
        <div className="dropdownContainer">
            <div className="dropdownLegend">
                Search by topic
            </div>
            <div className="dropdownSubText">
                {/*  */}
            </div>
            <div className="dropdownInput">
                <select
                    className="dropdownSelect" 
                    value={selectedCategory}
                    onChange={((e) => onOptSelect(e.target.value))}>
                    <option id='dropdownOpt'>Select</option>
                    {
                        categories?.map((opt) => {
                            return (
                                <option id='dropdownOpt' key={opt}>{opt}</option>
                            )
                        })
                    }
                </select>
            </div>
        </div>
    )
}

export default CustomDropdown;