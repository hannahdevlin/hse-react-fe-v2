import React, { useContext, useEffect, useState } from 'react';
import { ProductContext } from '../../../context/ProductContext';
import searchIcon from "../../../assets/svgs/search-btn-icon.svg"
import './style.css';

export default function SearchBar () {
const { setSearchTerm, searchTerm, selectedCategory } = useContext(ProductContext);
// Value that is passed to the button
const [localVal, setLocalVal] = useState('');

  useEffect(() => {
    if (searchTerm) {
      setLocalVal('');
      setSearchTerm('');
    }
  }, [selectedCategory]);

    return (
        <div className='searchBarContainer'>
            <div className="searchBarLegend">
                Search by keyword
            </div>
            <div style={{ display: 'flex' }}>
                <input
                  className='searchInput'
                  value={localVal}
                  onChange={(e) => setLocalVal(e.target.value)}
                  type='text'/>
                <img
                  src={searchIcon}
                  className='searchTermBtn'
                  alt="searchIcon"
                  onClick={() => setSearchTerm(localVal)}
                  style={{
                    width: 36,
                    marginLeft: 4,
                    cursor: !localVal.length ? 'not-allowed' : 'pointer'
                  }} />
            </div>
        </div>
    )
}