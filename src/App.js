import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import "./App.css";

import { connect } from "react-redux";

import Login from "./components/Login";
import Navbar from "./components/Navbar/Navbar";
import Products from "./components/Products/Products";
import Cart from "./components/Cart/Cart";
import UserDetails from "./components/User/UserDetails";
import EditUserDetails from "./components/User/EditUserDetails";
import OrderConfirmed from "./components/Cart/OrderConfirmed";

function App() {
  return (
    <Router>
      <div className="app">
        <Navbar />
        <Switch>
          <Route exact path="/login" component={Login} />
          <Route exact path="/products" component={Products} />
          <Route exact path="/cart" component={Cart} />
          <Route exact path="/user-details" component={UserDetails} />
          <Route exact path="/user-details-edit" component={EditUserDetails} />
          <Route exact path="/order-confirmed" component={OrderConfirmed} />
        </Switch>
      </div>
    </Router>
  );
}

const mapStateToProps = (state) => {
  return {
    current: state.shop.currentItem,
  };
};

export default connect(mapStateToProps)(App);
