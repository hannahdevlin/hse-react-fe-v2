import * as actionTypes from "./shopping-types";

const INITIAL_STATE = {
  products: [],
  categories: [],
  cart: [],
  currentItem: null,
  loading: false,
  error: '',
  selectedCategory: null
};

const shopReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {

    case actionTypes.FETCH_PRODUCTS_REQUEST: 
      return {
        ...state,
        loading: true
      }

    case actionTypes.FETCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        loading: false,
        products: action.payload,
        error: ''
      }

    case actionTypes.FETCH_PRODUCTS_FAIL:
      return {
        loading: false,
        products: [],
        error: action.payload
      }
    
    case actionTypes.FETCH_CATEGORIES:
      return {
        ...state,
        loading: false,
        categories: action.payload,
        error: ''
      }

    case actionTypes.ADD_TO_CART:
      const item = state.products.find(
        (product) => product.id === action.payload.id.id
      );
      const inCart = state.cart.find((item) =>
        item.id === action.payload.id.id ? true : false
      );

      return {
        ...state,
        cart: inCart
        // if item is in cart, increase qty by 1
          ? state.cart.map((item) =>
              item.id === action.payload.id.id
                ? { ...item, qty: item.qty + action.payload.id.input }
                : item
            )
        // if its not in the cart, add one to the cart
          : [...state.cart, { ...item, qty: action.payload.id.input }],
      };

    case actionTypes.REMOVE_FROM_CART:
      return {
        ...state,
        cart: state.cart.filter((item) => item.id !== action.payload.id),
      };

    case actionTypes.ADJUST_ITEM_QTY:
      return {
        ...state,
        cart: state.cart.map((item) =>
          item.id === action.payload.id
            ? { ...item, qty: +action.payload.qty }
            : item
        ),
      };

    case actionTypes.LOAD_CURRENT_ITEM:
      return {
        ...state,
        currentItem: action.payload,
      };

    default:
      return state;
  }
};

export default shopReducer;
